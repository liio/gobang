# Python 五子棋

#### 介绍
基于 Numpy 和 Tkinter 的 Python 简易五子棋
![思路（没有完全实现）](https://gitee.com/liio/gobang/raw/master/五子棋.png)
#### 依赖

`
pip install Numpy
pip install matplotlib
`

#### 运行
`python tk_game.py`

#### 截图
![运行截图](https://gitee.com/liio/gobang/raw/master/source/img/background_c.png)